# army battle game

## General:

A simple game made to test how to work with the bevy engine.

## Gameplay:

### Planned:
 - simple base building
 - simple resource management
 - No win in general, just surviving as long as possible
 - score based on farmed resources, killed enemies and technological advancements
 - different units for both sides
 - special units for both sides
 - player can control units
 - Enemies attack in bigger and bigger waves
 - random generated maps

### Finished:

## Game Features needed: 

- Gebäude
- Raycasts
- Win/Loose
- Better pathfinding
- Unit selection

## Gameplay feature details:

### Simple base building

The player can build a couple of different recruitment and production buildings as well as defensive buildings.
Buildings will be build automaticly over time. 
Buildings will cost resources.
Players have to place down buildings.
Players can destroy buildings.

## Buildings:

### Tower

A defensive building which can withstand alot of damage.
It can shoot arrows. 
Archers can be stationed in it.

#### Components:
 - Health
 - Defense
 - Building
 - Tower(enum)

- Base
    - Main building (upgrad)
    - Recruits:
        - Workers
 - Baracks
    - 
 - House
    - 