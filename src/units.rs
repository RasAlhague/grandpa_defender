use crate::DetectedCollision;
use bevy::prelude::*;

#[derive(Component)]
pub struct FreeGuard;

#[derive(Component)]
pub struct Grandpa;

#[derive(Component)]
pub struct Health {
    pub max: f32,
    pub current: f32,
}

#[derive(Component)]
pub struct Damage(pub f32);

#[derive(Component)]
pub struct MoveSpeed(pub f32);

#[derive(Component)]
pub struct AttackSpeed(pub Timer);

#[derive(Component)]
pub struct Defense(pub f32);

#[derive(Component)]
pub struct Unit;

#[derive(Bundle)]
pub struct GrandpaUnitBundle {
    pub _g: Grandpa,
    pub _u: Unit,
    pub health: Health,
    pub damage: Damage,
    pub defense: Defense,
    pub move_speed: MoveSpeed,
    pub attack_speed: AttackSpeed,

    #[bundle]
    pub sprite: SpriteBundle,
}

#[derive(Bundle)]
pub struct FreeGuardUnitBundle {
    pub _f: FreeGuard,
    pub _u: Unit,
    pub health: Health,
    pub damage: Damage,
    pub defense: Defense,
    pub move_speed: MoveSpeed,
    pub attack_speed: AttackSpeed,

    #[bundle]
    pub sprite: SpriteBundle,
}

pub enum HealthChange {
    Increase(Entity, f32),
    Decrease(Entity, f32),
}

pub fn damage_enemies(
    time: Res<Time>,
    mut detected_collisions: EventReader<DetectedCollision>,
    mut health_changes: EventWriter<HealthChange>,
    units: Query<(&Damage, &Defense, Option<&FreeGuard>, Option<&Grandpa>), With<Unit>>,
    mut unit_attack_speeds: Query<&mut AttackSpeed, With<Unit>>
) {
    for detected_collision in detected_collisions.iter() {
        if let Ok((damage, _, source_guard, source_grandpa)) = units.get(detected_collision.source) {
            if let Ok(mut speed) = unit_attack_speeds.get_mut(detected_collision.source) {
                if speed.0.tick(time.delta()).finished() {
                    if let Ok((_, defense, target_guard, target_grandpa)) = units.get(detected_collision.target) {
                        let concrete_damage = damage.0 - defense.0;
        
                        if (source_grandpa.is_some() && target_guard.is_some()) || (source_guard.is_some() && target_grandpa.is_some()) {
                            health_changes.send(HealthChange::Decrease(detected_collision.target, concrete_damage.max(0.0)));
                        }
                    }
    
                    speed.0.reset();
                }
            }       
        }
    }
} 

pub fn change_color(
    mut health_changes: EventReader<HealthChange>,
    mut sprites: Query<&mut Sprite, With<Unit>>,
) {
    for health_change in health_changes.iter() {
        match health_change {
            HealthChange::Increase(entity, _) => {
                if let Ok(mut sprite) = sprites.get_mut(*entity) {
                    sprite.color = Color::GREEN;
                }
            },
            HealthChange::Decrease(entity, _) => {
                if let Ok(mut sprite) = sprites.get_mut(*entity) {
                    sprite.color = Color::RED;
                }
            },
        }
    }
}

pub fn update_health(
    mut health_changes: EventReader<HealthChange>,
    mut deaths: EventWriter<Dead>,
    mut units: Query<&mut Health, With<Unit>>
) {
    for health_change in health_changes.iter() {
        match health_change {
            HealthChange::Increase(entity, amount) => {
                if let Ok(mut health) = units.get_mut(*entity) {
                    if health.current + amount > health.max {
                        health.current = health.max;
                    }
                    else {
                        health.current += amount;
                    } 
                }
            },
            HealthChange::Decrease(entity, amount) => {
                if let Ok(mut health) = units.get_mut(*entity) {
                    health.current -= amount;

                    if health.current < 0.0 {
                        deaths.send(Dead(*entity));
                    } 
                }
            },
        }
    }
}

pub struct Dead(Entity);

pub fn kill_units(
    mut commands: Commands,
    mut deaths: EventReader<Dead>,
    units: Query<Entity, With<Unit>>,
) {
    let mut despawned_entities = Vec::new();

    for death in deaths.iter() {
        if despawned_entities.contains(&death.0) {
            continue;
        }

        if let Ok(_) = units.get(death.0) {
            commands.entity(death.0).despawn();
            despawned_entities.push(death.0);
        }
    }
}