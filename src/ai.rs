use crate::units::{FreeGuard, Grandpa, MoveSpeed, Unit};
use bevy::prelude::*;
use bevy::tasks::ComputeTaskPool;

#[derive(Component)]
pub struct Target(pub Option<Vec3>);

pub fn find_targets(
    pool: Res<ComputeTaskPool>,
    time: Res<Time>,
    mut interval: ResMut<AiInterval>,
    mut fg_units: Query<(&mut Target, &Transform), (With<Unit>, With<FreeGuard>, Without<Grandpa>)>,
    mut grandpa_units: Query<
        (&mut Target, &Transform),
        (With<Unit>, With<Grandpa>, Without<FreeGuard>),
    >,
) {
    if interval.0.tick(time.delta()).finished() {
        set_targets(&pool, &mut fg_units, &grandpa_units);
        set_targets(&pool, &mut grandpa_units, &fg_units);

        interval.0.reset();
    }
}

fn set_targets<TWith: Component, TWithout: Component>(
    pool: &Res<ComputeTaskPool>,
    query1: &mut Query<(&mut Target, &Transform), (With<Unit>, With<TWith>, Without<TWithout>)>,
    query2: &Query<(&mut Target, &Transform), (With<Unit>, With<TWithout>, Without<TWith>)>,
) {
    query1.par_for_each_mut(&pool, 32, |(mut target, first_transform)| {
        let mut closest: Option<Vec3> = None;
        let mut closest_distance: Option<f32> = None;

        for (_, other_transform) in query2.iter() {
            let distance = first_transform
                .translation
                .distance(other_transform.translation)
                .abs();

            if let Some(close_dist) = closest_distance {
                if distance < close_dist {
                    closest_distance = Some(distance);
                    closest = Some(other_transform.translation);
                }
            } else {
                closest_distance = Some(distance);
                closest = Some(other_transform.translation);
            }
        }

        if let Some(vec) = closest {
            target.0 = Some(vec);
        } else {
            target.0 = None;
        }
    });
}

pub fn move_towards_target(
    time: Res<Time>,
    mut query: Query<(&mut Transform, &MoveSpeed, &Target), With<Unit>>,
) {
    for (mut transform, speed, target) in query.iter_mut() {
        if let Some(target_pos) = target.0 {
            let direction = target_pos - transform.translation;
            let direction = direction.normalize();

            transform.translation.x += direction.x * speed.0 * time.delta_seconds();
            transform.translation.y += direction.y * speed.0 * time.delta_seconds();
        }
    }
}

pub struct AiInterval(Timer);

impl Default for AiInterval {
    fn default() -> Self {
        AiInterval(Timer::from_seconds(0.1, false))
    }
}
