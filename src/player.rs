use bevy::prelude::*;

#[derive(Component)]
pub struct Player {
    pub name: String,
}

#[derive(Component)]
pub struct Score {
    pub value: usize,
}

pub fn update_score() {}
