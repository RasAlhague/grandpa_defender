use crate::collision::BoxCollider;
use crate::collision::Collider;
use crate::ai::Target;
use crate::units::*;
use rand::Rng;
use crate::units::Grandpa;
use bevy::prelude::*;

#[derive(Component)]
pub struct SpawnPoint;

#[derive(Component)]
pub struct SpawnInterval(pub Timer);

#[derive(Component)]
pub struct SpawnAmount(pub u32);

#[derive(Component)]
pub struct Active;

#[derive(Bundle)]
pub struct GrandpaSpawnPoint {
    pub _g: Grandpa,
    pub _p: SpawnPoint,
    pub interval: SpawnInterval,
    pub amount: SpawnAmount,
    pub transform: Transform,
}

pub fn spawn_grandpas(
    time: Res<Time>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
    mut spawn_points: Query<(&SpawnAmount, &Transform, &mut SpawnInterval), (With<SpawnPoint>, (With<Grandpa>, With<Active>))>,
) {
    let mut rng = rand::thread_rng();

    for (spawn_amount, spawn_center, mut spawn_interval) in spawn_points.iter_mut() {
        let texture_handle = asset_server.load("textures/units/grandpa.png");

        if spawn_interval.0.tick(time.delta()).finished() {
            spawn_interval.0.reset();

            for _ in 0..spawn_amount.0 {
                let mut new_transform = Transform::from_translation(spawn_center.translation);
                new_transform.translation.x += rng.gen_range(-200.0..200.0);
                new_transform.translation.y += rng.gen_range(-200.0..200.0);
                
                new_transform.scale = Vec3::new(3.0, 3.0, 1.0);
                
                commands
                .spawn_bundle(GrandpaUnitBundle {
                    _u: Unit,
                    _g: Grandpa,
                    health: Health {
                        max: 100.0,
                        current: 100.0,
                    },
                    damage: Damage(100.0),
                    defense: Defense(5.0),
                    move_speed: MoveSpeed(70.0),
                    attack_speed: AttackSpeed(Timer::from_seconds(0.7, false)),
                    sprite: SpriteBundle {
                        texture: texture_handle.clone(),
                        transform: new_transform,
                        sprite: Sprite {
                            flip_x: true,
                            flip_y: false,
                            ..Default::default()
                        },
                        ..Default::default()
                    },
                })
                .insert(Target(None))
                .insert(Collider)
                .insert(BoxCollider {
                    top_left: Vec2::new(0.0, 0.0),
                    width: 10.0,
                    height: 8.0,
                });
            }
        }
    }
}