extern crate serde_json;

use std::collections::HashMap;
use crate::collision::BoxCollider;
use crate::collision::Collider;
use crate::ai::Target;
use crate::units::*;
use crate::UnitCounter;
use bevy::prelude::*;
use rand::Rng;
use serde::{Deserialize, Serialize};
use crate::key_mappings::KeyMapping;

#[derive(Deserialize, Serialize)]
pub struct InputMapping {
    mapping: HashMap<String, KeyMapping>,
}

impl Default for InputMapping {
    fn default() -> Self {
        InputMapping::new()
    }
}

impl InputMapping {
    pub fn new() -> Self {
        InputMapping {
            mapping: HashMap::new(),
        }
    }

    pub fn add_mapping(mut self, key: &str, mapping: KeyMapping) -> Self {
        self.mapping.insert(String::from(key), mapping);
        self
    }

    pub fn check_just_pressed(&self, input_name: &str, input: &Res<Input<KeyCode>>) -> bool {
        if let Some(mapping) = self.mapping.get(input_name) {
            let code = mapping.to_key_code();

            return input.just_pressed(code);
        }

        false
    }

    pub fn check_pressed(&self, input_name: &str, input: &Res<Input<KeyCode>>) -> bool {
        if let Some(mapping) = self.mapping.get(input_name) {
            let code = mapping.to_key_code();

            return input.pressed(code);
        }

        false
    }

    pub fn parse_json(json: &str) -> serde_json::Result<Self> {
        let mapping = serde_json::from_str(json)?;

        Ok(mapping)
    }
}

pub struct CameraMoved {
    pub new_position: Transform, 
    pub moved_camera: Entity 
}

pub fn move_camera(
    time: Res<Time>,
    keys: Res<Input<KeyCode>>,
    mapping: Res<InputMapping>,
    mut camera_moves: EventWriter<CameraMoved>,
    mut query: Query<(Entity, &mut Transform), With<Camera>>,
) {
    let mut x = 0.0;
    let mut y = 0.0;

    if mapping.check_pressed("camera_upwards", &keys) {
        y = 5.0;
    }
    if mapping.check_pressed("camera_downwards", &keys) {
        y = -5.0;
    }
    if mapping.check_pressed("camera_left", &keys) {
        x = -5.0;
    }
    if mapping.check_pressed("camera_right", &keys) {
        x = 5.0;
    }

    for (camera, mut transform) in query.iter_mut() {
        transform.translation.y += y * 60.0 * time.delta_seconds();
        transform.translation.x += x * 60.0 * time.delta_seconds();
        camera_moves.send(CameraMoved {
            new_position: transform.clone(),
            moved_camera: camera,
        })
    }
}

pub fn handle_spawn_input(
    keys: Res<Input<KeyCode>>,
    asset_server: Res<AssetServer>,
    mut unit_counter: ResMut<UnitCounter>,
    mut commands: Commands,
) {
    if keys.just_pressed(KeyCode::Q) {
        let mut rng = rand::thread_rng();
        let texture_handle = asset_server.load("textures/units/free_guard.png");

        for _ in 0..100 {
            let mut new_transform = Transform::from_scale(Vec3::splat(1.0));
            new_transform.translation.x = rng.gen_range(-1000.0..100.0);
            new_transform.translation.y = rng.gen_range(-1000.0..1000.0000);
            new_transform.scale = Vec3::new(3.0, 3.0, 1.0);

            commands
                .spawn_bundle(FreeGuardUnitBundle {
                    _u: Unit,
                    _f: FreeGuard,
                    health: Health {
                        max: 100.0,
                        current: 100.0,
                    },
                    damage: Damage(50.0),
                    defense: Defense(20.0),
                    move_speed: MoveSpeed(50.0),
                    attack_speed: AttackSpeed(Timer::from_seconds(0.5, false)),
                    sprite: SpriteBundle {
                        texture: texture_handle.clone(),
                        transform: new_transform,
                        ..Default::default()
                    },
                })
                .insert(Target(None))
                .insert(Collider)
                .insert(BoxCollider {
                    top_left: Vec2::new(0.0, 0.0),
                    width: 11.0,
                    height: 8.0,
                });

            unit_counter.0 += 1;
        }
    }
    if keys.just_pressed(KeyCode::E) {
        let mut rng = rand::thread_rng();
        let texture_handle = asset_server.load("textures/units/grandpa.png");

        for _ in 0..100 {
            let mut new_transform = Transform::from_scale(Vec3::splat(1.0));
            new_transform.translation.x = rng.gen_range(-100.0..1000.0);
            new_transform.translation.y = rng.gen_range(-1000.0..1000.0);

            new_transform.scale = Vec3::new(3.0, 3.0, 1.0);

            commands
                .spawn_bundle(GrandpaUnitBundle {
                    _u: Unit,
                    _g: Grandpa,
                    health: Health {
                        max: 100.0,
                        current: 100.0,
                    },
                    damage: Damage(100.0),
                    defense: Defense(5.0),
                    move_speed: MoveSpeed(70.0),
                    attack_speed: AttackSpeed(Timer::from_seconds(0.7, false)),
                    sprite: SpriteBundle {
                        texture: texture_handle.clone(),
                        transform: new_transform,
                        sprite: Sprite {
                            flip_x: true,
                            flip_y: false,
                            ..Default::default()
                        },
                        ..Default::default()
                    },
                })
                .insert(Target(None))
                .insert(Collider)
                .insert(BoxCollider {
                    top_left: Vec2::new(0.0, 0.0),
                    width: 10.0,
                    height: 8.0,
                });

            unit_counter.0 += 1;
        }
    }
}

