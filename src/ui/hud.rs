use crate::input::CameraMoved;
use bevy::prelude::*;

#[derive(Component)]
pub struct Hud;

#[derive(Component)]
pub struct BuildingBar;

#[derive(Component)]
pub struct UnitBar;

#[derive(Component)]
pub struct ResourceBar;

#[derive(Component)]
pub struct TimerBox;

#[derive(Component)]
pub struct MenuBox;

#[derive(Component)]
pub struct StatisticsBox;

pub fn setup_hud(
    mut commands: Commands, 
    asset_server: Res<AssetServer>
) {
    create_full_hud(commands, asset_server);
}

pub fn create_full_hud(
    mut commands: Commands, 
    asset_server: Res<AssetServer>
) {
    let root_node = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                justify_content: JustifyContent::SpaceBetween,
                ..Default::default()
            },
            color: Color::rgb(0.15, 0.15, 0.15).into(),
            ..Default::default()
        })
        .insert(Hud)
        .id();
}

fn create_resource_bar(
    mut commands: Commands, 
    asset_server: Res<AssetServer>
) {

}

pub fn move_hud(
    mut camera_moves: EventReader<CameraMoved>,
    mut hud: Query<(&mut Transform, &Node), With<Hud>>,
) {
    for camera_move in camera_moves.iter() {
        for (mut hud_transform, node) in hud.iter_mut() {
            hud_transform.translation.x = camera_move.new_position.translation.x;
            hud_transform.translation.y = camera_move.new_position.translation.y;
        }  
    }
}