use bevy::prelude::*;

pub enum MenuState {
    MainMenu,
    OptionMenu,
    PauseMenu,
    SaveMenu,
    LoadMenu,
}

impl Default for MenuState {
    fn default() -> Self {
        MenuState::MainMenu
    }
}

#[derive(Component)]
pub struct Menu;

#[derive(Component)]
pub struct MainMenu;

#[derive(Component)]
pub struct PauseMenu;

#[derive(Component)]
pub struct OptionMenu;

#[derive(Component)]
pub struct SaveMenu;

#[derive(Component)]
pub struct LoadMenu;


// fn create_main_menu(
//     mut commands: Commands, 
//     asset_server: Res<AssetServer>
// ) -> Entity {
//     commands
//         .spawn_bundle(NodeBundle {
//             style: Style {
//                 size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
//                 justify_content: JustifyContent::SpaceBetween,
//                 ..Default::default()
//             },
//             color: Color::rgb(1.0, 1.0, 1.0).into(),
//             ..Default::default()
//         })
//         .insert(MainMenu)
//         .insert(Menu)
//         .with_children(|parent|{
//             parent
//                 .spawn_bundle()
//         })
//         .id()   
// }

// fn create_options_menu(
//     mut commands: Commands, 
//     asset_server: Res<AssetServer>
// ) -> Entity {
//     commands
//         .spawn_bundle()
//         .id() 
// }

// fn create_pause_menu(
//     mut commands: Commands, 
//     asset_server: Res<AssetServer>
// ) -> Entity {
//     commands
//         .spawn_bundle()
//         .id() 
// }