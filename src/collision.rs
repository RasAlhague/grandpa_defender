use bevy::prelude::*;

#[derive(Component)]
pub struct Collider;

#[derive(Component)]
pub struct Trigger;

#[derive(Component)]
pub struct BoxCollider {
    pub top_left: Vec2,
    pub width: f32,
    pub height: f32,
}

impl BoxCollider {
    fn check_collision(&self, own_position: &Transform, other_collider: &BoxCollider, other_position: &Transform) -> bool {
        let own_position = own_position.translation + Vec3::new(self.top_left.x, self.top_left.y, 0.0);
        let other_position = other_position.translation + Vec3::new(other_collider.top_left.x, other_collider.top_left.y, 0.0);
 
        own_position.x < (other_position.x + other_collider.width) &&
        own_position.x + self.width > other_position.x &&
        own_position.y < (other_position.y + other_collider.height) &&
        own_position.y + self.height > other_position.y
    }
}

pub struct DetectedCollision {
    pub source: Entity,
    pub target: Entity,
}

pub fn check_box_collision(
    colliders: Query<(Entity, &BoxCollider, &Transform), (With<Collider>, Without<Trigger>)>, 
    mut detected_collisions: EventWriter<DetectedCollision>,
) {
    for (entity, collider, transform) in colliders.iter() {
        for (other_entity, other_collider, other_transform) in colliders.iter() {
            if entity == other_entity {
                continue;
            }

            if collider.check_collision(transform, other_collider, other_transform) {
                detected_collisions.send(DetectedCollision {
                    source: entity,
                    target: other_entity,
                });
            }
        }
    }
}