mod ai;
mod collision;
mod input;
mod player;
mod spawning;
mod units;
mod buildings;
mod ui;
mod key_mappings;

use std::path::Path;
use crate::input::InputMapping;
use crate::units::Grandpa;
use crate::spawning::*;
use crate::units::Dead;
use crate::units::HealthChange;
use crate::collision::DetectedCollision;
use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::prelude::*;

static INPUT_CONFIG_FILE: &str = "./config/input.json";

fn main() {
    let input_mapping = match load_input_mapping(Path::new(INPUT_CONFIG_FILE)) {
        Ok(mapping) => mapping,
        Err(err) => {
            println!("Err while loading input file. Creating default input.");
            println!("Err: {}", err);

            let mapping = InputMapping::new()
                .add_mapping("camera_upwards", crate::key_mappings::KeyMapping::W)
                .add_mapping("camera_downwards", crate::key_mappings::KeyMapping::S)
                .add_mapping("camera_left", crate::key_mappings::KeyMapping::A)
                .add_mapping("camera_right", crate::key_mappings::KeyMapping::D)
                .add_mapping("select_main_building", crate::key_mappings::KeyMapping::Q)
                .add_mapping("select_barracks", crate::key_mappings::KeyMapping::E);

            save_input_mapping(Path::new(INPUT_CONFIG_FILE), &mapping).expect("Saving input mapping failed!");

            mapping
        },
    };

    App::new()
        .insert_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
        .add_plugins(DefaultPlugins)
        .add_plugin(LogDiagnosticsPlugin::default())
        .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .add_event::<DetectedCollision>()
        .add_event::<HealthChange>()
        .add_event::<Dead>()
        .add_event::<input::CameraMoved>()
        .insert_resource(Msaa { samples: 4 })
        .insert_resource(input_mapping)
        .init_resource::<UnitCounter>()
        .init_resource::<ai::AiInterval>()
        .add_startup_system(setup)
        .add_startup_system(setup_granda_spawns)
        .add_startup_system(ui::hud::setup_hud)
        .add_system(spawning::spawn_grandpas)
        .add_system(collision::check_box_collision)
        .add_system(units::damage_enemies)
        .add_system(units::change_color)
        .add_system(units::update_health)
        .add_system(ai::find_targets)
        .add_system(ai::move_towards_target)
        .add_system(input::move_camera)
        .add_system(ui::hud::move_hud)
        .add_system(input::handle_spawn_input)
        .add_system(units::kill_units)
        .run();
}

fn load_input_mapping(path: &Path) -> std::io::Result<InputMapping> {
    let json = std::fs::read_to_string(path)?;
    let mapping: InputMapping = serde_json::from_str(&json)?;

    Ok(mapping)
}

fn save_input_mapping(path: &Path, mapping: &InputMapping) -> std::io::Result<()> {
    let json = serde_json::to_string(&mapping)?;
    std::fs::write(path, json)?;

    Ok(())
}

#[derive(Default)]
pub struct UnitCounter(pub u64);

fn setup(mut commands: Commands) {
    commands.spawn_bundle(UiCameraBundle::default());
    commands
        .spawn_bundle(OrthographicCameraBundle::new_2d());
}

fn setup_granda_spawns(
    mut commands: Commands
) {
    commands
        .spawn_bundle(GrandpaSpawnPoint {
            _p: SpawnPoint,
            _g: Grandpa,
            interval: SpawnInterval(Timer::from_seconds(10.0, false)),
            amount: SpawnAmount(33),
            transform: Transform::from_xyz(500.0, 500.0, 0.0),
        })
        .insert(Active);
    commands
        .spawn_bundle(GrandpaSpawnPoint {
            _p: SpawnPoint,
            _g: Grandpa,
            interval: SpawnInterval(Timer::from_seconds(10.0, false)),
            amount: SpawnAmount(33),
            transform: Transform::from_xyz(500.0, 0.0, 0.0),
        });
    commands
        .spawn_bundle(GrandpaSpawnPoint {
            _p: SpawnPoint,
            _g: Grandpa,
            interval: SpawnInterval(Timer::from_seconds(10.0, false)),
            amount: SpawnAmount(33),
            transform: Transform::from_xyz(500.0, -500.0, 0.0),
        })
        .insert(Active);
}