use bevy::prelude::*;

#[derive(Component)]
pub struct Building;

#[derive(Component)]
pub enum Tower {
    Bow,
    Crossbow,
    Ballista,
}

#[derive(Component)]
pub struct MainBase;

#[derive(Component)]
pub struct House;

#[derive(Component)]
pub struct Mine;

#[derive(Component)]
pub struct Field; 
